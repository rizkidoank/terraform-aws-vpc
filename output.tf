output "vpc_id" {
  description = "The ID of the VPC."
  value       = aws_vpc.this.id
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC."
  value       = aws_vpc.this.cidr_block
}

output "vpc_enable_dns_support" {
  description = "Whether or not the VPC has DNS support."
  value       = aws_vpc.this.enable_dns_support
}

output "vpc_enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support."
  value       = aws_vpc.this.enable_dns_hostnames
}

output "subnet_public_ids" {
  description = "List of IDs of public subnets."
  value       = aws_subnet.public[*].id
}

output "subnet_public_cidr_blocks" {
  description = "List of cidr_blocks of public subnets."
  value       = aws_subnet.public[*].cidr_block
}

output "subnet_private_ids" {
  description = "List of IDs of private subnets."
  value       = aws_subnet.private[*].id
}

output "subnet_private_cidr_blocks" {
  description = "List of cidr_blocks of private subnets."
  value       = aws_subnet.private[*].cidr_block
}

output "igw_id" {
  description = "The ID of the Internet Gateway."
  value       = aws_internet_gateway.this.id
}

output "eip_nat_ids" {
  description = "List of Elastic IP allocation IDs for NAT Gateway."
  value       = aws_eip.nat[*].id
}

output "eip_nat_public_ips" {
  description = "List of Elastic IP  public IPs for NAT Gateway."
  value       = aws_eip.nat[*].public_ip
}

output "nat_ids" {
  description = "List of NAT Gateway IDs"
  value       = aws_nat_gateway.this[*].id
}

output "nat_private_ips" {
  description = "List of private IP addresses of the NAT Gateway."
  value       = aws_nat_gateway.this[*].private_ip
}

output "rtb_public_id" {
  description = "ID of public route table"
  value       = aws_route_table.public.id
}

output "rtb_private_ids" {
  description = "List of IDs of app route tables"
  value       = aws_route_table.private[*].id
}
