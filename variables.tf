variable "environment" {
  description = "Environment where the VPC provisioned"
  type        = string
}

variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
}

variable "vpc_description" {
  description = "Description of the VPC"
  type        = string
}

variable "vpc_cidr_block" {
  description = "CIDR block reserved for the VPC"
  type        = string
}

variable "subnet_availability_zones" {
  description = "List of subnets availability zones"
  type        = list
}

variable "cidr_added_bits" {
  description = "Added bits for the CIDR block for subnet calculation"
  type        = number
}

variable "vpc_enable_dns_support" {
  description = "Enable DNS support for the VPC"
  default     = true
  type        = bool
}

variable "vpc_enable_dns_hostnames" {
  description = "Enable DNS hostnames for the VPC"
  default     = true
  type        = bool
}